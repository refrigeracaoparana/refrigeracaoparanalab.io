---
title: "Contato"
weight: 40
header_menu: true
---

Falar com **Vando**

{{<icon class="fa fa-whatsapp">}}&nbsp;[(47) 99128-2066](https://wa.me/5547991282066)

{{<icon class="fa fa-map-marker">}}&nbsp;Av. Celso Ramos, 2234, Itapoá - SC

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3583.906713469323!2d-48.61962124898145!3d-26.06930496475368!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94d9564196b154c1%3A0xdd87c57235a30586!2zUmVmcmlnZXJhw6fDo28gUGFyYW7DoSDimI7vuI8gKDQ3KSA5OTEyOCAyMDY2!5e0!3m2!1spt-BR!2sbr!4v1638749476016!5m2!1spt-BR!2sbr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

![Contato](images/qrcode.png)
<p align="center">Escaneio o QR code para adicionar aos contatos </p>
